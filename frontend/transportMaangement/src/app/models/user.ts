import { Branch } from "./branch";

export class User {

    id?: any;
    firstname?: string;
    lastname?: string;
     username?: string;
    password?: string;
    email?: string;
    branch?:Branch;
}
