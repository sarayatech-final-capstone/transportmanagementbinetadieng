import { Branch } from "./branch";
import { Driver } from "./driver";

export class Truck {

      id?: any;
    model?: string;
    no?: string;
    insurance?: string;
    capacity?:number;
    status?: boolean;
    branch?: Branch;
    driver?: Driver;
}
