import { Truck } from "./truck";

export class Driver {
    id?: any;
    name?: string;
    email?: string;
    phone?:string;
    address?:string;
    truck?: Truck;
}
