import { User } from "./user";

export class Branch {
  


    id?: any;
    name?: string;
    country?: string;
    city?: string;
    manager?: User;
}
