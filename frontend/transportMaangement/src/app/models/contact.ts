export class Contact {


    id?: any;
    name?: string;
    email?: string;
    phone?: string;
    message?: string;
}
